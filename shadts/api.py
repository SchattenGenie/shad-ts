import datetime
import functools
import redis
import hmac

from flask import request, abort, render_template, session, redirect, url_for

from . import app, make_gitlab_api, url_for_student_repo
from .gdoc import RatingsTable
from .course import Deadlines
from .login import requires_auth


def requires_token(f):
    @functools.wraps(f)
    def decorated(*args, **kwargs):
        token = request.form.get("token", "")
        if token != app.config["TESTER_TOKEN"]:
            abort(403)

        return f(*args, **kwargs)
    return decorated


@app.route("/api/sync_task_columns", methods=["POST"])
@requires_token
def sync_task_columns():
    deadlines = Deadlines.fetch()
    rt = RatingsTable.from_config()
    rt.sync_task_columns(deadlines.task_names)
    return "", 200


def mark_solved(username, task):
    client = redis.StrictRedis(app.config["REDIS"])
    client.hset("solved:{}:{}".format(app.config["COURSE_NAME"], username), task, "1")


def save_attempt(username, task):
    client = redis.StrictRedis(app.config["REDIS"])
    return client.hincrby("attempts:{}:{}".format(app.config["COURSE_NAME"], username), task, 1)


def get_attempt(username, task):
    client = redis.StrictRedis(app.config["REDIS"])
    attempt = client.hget("attempts:{}:{}".format(app.config["COURSE_NAME"], username), task)
    if attempt is not None:
        return int(attempt)
    else:
        return 0


def accept_submit(user, task):
    student = {
        "username": user.username,
        "repo": url_for_student_repo(user.username),
        "name": user.name,
        "city": ""
    }

    rt = RatingsTable.from_config()
    attempt = get_attempt(user.username, task.name)

    def update_score(flags, old_score):
        extra_time = datetime.timedelta()
        if "z" in flags:
            extra_time = datetime.timedelta(weeks=1)

        if task.is_overdue(extra_time) or not task.has_attempts_left(attempt):
            new_score = int(app.config["COURSE_DEADLINE_PENALTY"] * task.score)
        else:
            new_score = task.score

        return max(new_score, old_score)

    rt.put_score(student, task.name, update_score)
    mark_solved(user.username, task.name)


@app.route("/api/report", methods=["POST"])
@requires_token
def report():
    deadlines = Deadlines.fetch()
    task = deadlines.find_task(request.form["task"])

    user = make_gitlab_api().users.get(int(request.form["user_id"]))
    if "failed" in request.form:
        save_attempt(user.username, task.name)
        return "", 200

    accept_submit(user, task)

    return "", 200


def decode_flag(flag):
    msg, sig = flag[5:-1].rsplit(':', 1)
    challenge, date = msg.split(":", 1)

    if sig != hmac.new(app.config["CRASHME_KEY"].encode("utf8"), msg=msg.encode("utf8")).hexdigest():
        raise ValueError("invalid signature")

    return challenge, date


@app.route("/submit", methods=["GET", "POST"])
@requires_auth
def submit():
    deadlines = Deadlines.fetch()

    if request.method == "GET":
        return render_template("submit.html")

    flag = request.form["flag"]

    try:
        task_name, date = decode_flag(flag)
    except ValueError as ex:
        return render_template("submit.html", error_message="Invalid flag signature")

    user = make_gitlab_api().users.get(int(session["gitlab"]["id"]))
    task = deadlines.find_task(task_name)

    accept_submit(user, task)

    return redirect(url_for("main_page"))